# skeleton-api-fastify

Skeleton API project using fastify as the server framework. 

Fastify's documentation can be found here - [Getting Started with Fastify](https://www.fastify.io/docs/latest/Getting-Started/)

## Middleware
### Helmet 
This skeleton comes with [Helmet](https://www.npmjs.com/package/helmet). Helmet's documentation can be found here [Helmet Documenation](https://helmetjs.github.io/docs/)

## SSL Setup
Run the `generate-ssl.sh` script in the `scripts` folder to create the `.key` and `.cert` file for use with fastify's server options

```bash
$ sh ./scripts/generate-ssl.sh
```
