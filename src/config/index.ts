const { env } = process;

const HOST = env.HOST || "0.0.0.0";
const PORT = Number(env.PORT) || 3000;

const USE_SSL = env.USE_SSL ? true : undefined;
const USE_HTTP2: true | undefined = env.USE_HTTP2 ? true : undefined;
const ALLOW_HTTP1 = env.ALLOW_HTTP1 ? true : undefined;
const SSL_KEY = env.SSL_KEY || "../ssl/server.key";
const SSL_CERT = env.SSL_CERT || "../ssl/server.cert";

export default {
  HOST,
  PORT,
  USE_SSL,
  USE_HTTP2,
  ALLOW_HTTP1,
  SSL_KEY,
  SSL_CERT
}
