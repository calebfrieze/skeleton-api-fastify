import { RequestHandler } from "fastify";
import getHelloWorldMessage from "./getHelloWorldMessage";

export const helloWorldRouteSchema = {
  response: {
    200: {
      type: 'object',
      properties: {
        hello: { type: 'string' }
      }
    }
  }
};

export const helloWorldRouteHandler: RequestHandler = async (req, res) => {
  const message = getHelloWorldMessage();
  res.code(200).send(message);
}
