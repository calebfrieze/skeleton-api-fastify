import { RouteOptions } from "fastify";
import { helloWorldRouteHandler, helloWorldRouteSchema } from "./route";

const route: RouteOptions = {
  method: "GET",
  url: "/",
  schema: helloWorldRouteSchema,
  handler: helloWorldRouteHandler
}

export default route
