const getHelloWorldMessage = () => ({
  hello: 'world'
});

export default getHelloWorldMessage;
