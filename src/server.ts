import fs from "fs";
import path from "path";
import fastify, { ServerOptionsAsSecureHttp2, FastifyInstance } from "fastify";
import helmet from "fastify-helmet";
import config from "./config";
import helloWorldRoute from "./components/helloWorld";

let serverConfig: Partial<ServerOptionsAsSecureHttp2> = {
  logger: true
};

if (config.USE_SSL) {
  serverConfig = {
    ...serverConfig,
    http2: config.USE_HTTP2,
    https: {
      allowHTTP1: config.ALLOW_HTTP1,
      key: fs.readFileSync(path.resolve(__dirname, config.SSL_KEY)),
      cert: fs.readFileSync(path.resolve(__dirname, config.SSL_CERT))
    }
  }
}

const app: FastifyInstance = fastify(serverConfig);
app.register(helmet);
app.route(helloWorldRoute);

const start = async () => {
  try {
    await app.listen(config.PORT, config.HOST);
  } catch (e) {
    app.log.error(e)
    process.exit(1);
  }
}

export default {
  start
}
